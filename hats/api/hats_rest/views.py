from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.


class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet",
        "section",
        "shelf",
        "import_href",
    ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "pic_url",
        "location",
    ]

@require_http_methods(["GET", "POST"])
def api_hats_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location specified"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )

def api_hats_detail(request):
    pass
