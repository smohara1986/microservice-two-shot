from django.db import models

# Create your models here.


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    pic_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        "LocationVO",
        related_name="location",
        on_delete=models.CASCADE,
    )


class LocationVO(models.Model):
    closet = models.CharField(max_length=200, unique=True)
    section = models.PositiveSmallIntegerField()
    shelf = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=100)
